#pragma once
#include "afxwin.h"
#include<vector>

class PaintRefSig : public CStatic
{
private:
	// ������ ��� ������������� GDI+
	ULONG_PTR token;

	double xmin, xmax,   // ����������� � ������������ �������� �� �
		ymin, ymax,   // ����������� � ������������ �������� �� �
		step_x, step_y;   // ��� �� � � �
public:
	PaintRefSig();   // �����������
	~PaintRefSig();   // ����������

	std::vector < double > Vec_RefSig;   // ������ �������� �������� �������

	// ������� ������ ����������� ����� ��������
	void Exchange(double left, double right, double low, double up, double step_setka_x, double step_setka_y);

	// ������� �������������� � � ����������� �������
	double Trans_X(LPDRAWITEMSTRUCT lpDrawItemStruct, double x);
	// ������� �������������� � � ����������� �������
	double Trans_Y(LPDRAWITEMSTRUCT lpDrawItemStruct, double y);
	// ������� ���������
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
};