
// CrossCorrelationDlg.cpp : ���� ����������
//

#include "stdafx.h"
#include "CrossCorrelation.h"
#include "CrossCorrelationDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include"time.h"
#define _USE_MATH_DEFINES
#include"math.h"

// ���������� ���� CCrossCorrelationDlg



CCrossCorrelationDlg::CCrossCorrelationDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_CROSSCORRELATION_DIALOG, pParent)
	, _N1(200)
	, _N2(400)
	, _A(1)   // ���������
	, _fo(25)   // �������(�������) � ����������
	, _Fs(250)   // ������� ������������� � ����������
	, _Br(9600)
	, _d(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCrossCorrelationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PIC_REFSIG, PntRefSig);
	DDX_Text(pDX, IDC_EDIT1_N, _N1);
	DDX_Text(pDX, IDC_EDIT1_N2, _N2);
	DDX_Text(pDX, IDC_EDIT2_A, _A);
	DDX_Text(pDX, IDC_EDIT3_fo, _fo);
	DDX_Control(pDX, IDC_PIC_EXPLRSIG, PntExplrSig);
	DDX_Control(pDX, IDC_PIC_EXPLRSIG, PntExplrSig);
	DDX_Text(pDX, IDC_EDIT1_Br, _Br);
	DDX_Control(pDX, IDC_PIC_CROSSCRRLTN, PntCrssCrrltn);
	DDX_Control(pDX, IDC_CHECK1_BPSK, check_BPSK);
	DDX_Control(pDX, IDC_CHECK2_MSK, check_MSK);
}

BEGIN_MESSAGE_MAP(CCrossCorrelationDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTNSIG, &CCrossCorrelationDlg::OnBnClickedBtnsig)
END_MESSAGE_MAP()


// ����������� ��������� CCrossCorrelationDlg

BOOL CCrossCorrelationDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ������ ������ ��� ����� ����������� ����.  ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������

	srand(clock());

	_T = 1. / _Br;

	_xminSig1 = 0; _xmaxSig1 = _N1;
	_yminSig1 = -1.5; _ymaxSig1 = 1.5;
	PntRefSig.Exchange(_xminSig1, _xmaxSig1, _yminSig1, _ymaxSig1, _xmaxSig1 / 8, _ymaxSig1 / 3);

	_xminSig2 = 0; _xmaxSig2 = _N2;
	_yminSig2 = -1.5; _ymaxSig2 = 1.5;
	PntExplrSig.Exchange(_xminSig2, _xmaxSig2, _yminSig2, _ymaxSig2, _xmaxSig2 / 8, _ymaxSig2 / 3);

	_xminSig3 = 0; _xmaxSig3 = _N2;
	_yminSig3 = -1.5; _ymaxSig3 = 1.5;
	PntCrssCrrltn.Exchange(_xminSig3, _xmaxSig3, _yminSig3, _ymaxSig3, _xmaxSig3 / 8, _ymaxSig3 / 3);

	UpdateData(FALSE);

	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������.  ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ��������.

void CCrossCorrelationDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR CCrossCorrelationDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

/** ������������� ������������ �������(������� � �����������).
* ������� ����������� ����� ������������������ ���(0 � 1).*/
void CCrossCorrelationDlg::CreateBitSignals()
{
	vec_refsig.clear();
	vec_refsig.resize(_N1);
	float temp_value = 0;

	///////// ����� �����: vec_refsig[i] = rand() % 2;
	// ������� ������
	for (int i = 0; i < _N1; i++)
	{
		temp_value = (float)rand() / RAND_MAX;
		if (temp_value < 0.5)
			vec_refsig[i] = 0;
		else
			vec_refsig[i] = 1;
	}

	vec_explrsig.clear();
	vec_explrsig.resize(_N2);
	////// ��������� �������� ������ ����������
	int startRefSig = _N2 / 4;
	// ����������� ������
	////// ���� ����� �����:  std::copy(vec_refsig.begin(), vec_refsig.end(), vec_explrsig.begin() + startRefSig);
	for (int i = 0; i < _N2; i++)
	{
		if (i == startRefSig)
		{
			for (int j = 0; j < _N1; j++)
			{
				vec_explrsig[i] = vec_refsig[j];
				i++;
			}
		}

		temp_value = (float)rand() / RAND_MAX;
		if (temp_value < 0.5)
			vec_explrsig[i] = 0;
		else
			vec_explrsig[i] = 1;
	}

}

/** �������� ��������� BPSK.*/
void CCrossCorrelationDlg::ModulationBPSK()
{
	/** ���������� ����� � ����� ����.*/
	int amnt_pnt = (int)(_T * _Fs * 1000);
	PntRefSig.Vec_RefSig.clear();
	PntRefSig.Vec_RefSig.resize(_N1 * amnt_pnt);
	// ������� ������ � ���������� ��2
	for (int i = 0; i < _N1; i++)
	{
		if (vec_refsig[i] == true)
			for (int j = 0; j < amnt_pnt; j++)
				PntRefSig.Vec_RefSig[amnt_pnt * i + j] =
				///// ����� ������������ ������ amnt_pnt �� �����������
				_A * cos(2 * M_PI * (_fo * 1000) * ((amnt_pnt * i + j) / (_Fs * 1000 * amnt_pnt)) + M_PI);

			/////// ������, �������� ������� �����, ��� ��� ������� ��������� ���� ������� sin � cos
			////// �������� �������� �����������. ������� ��������� ���� ��������� �� ���� ������ ����� 2*Pi ������� 
			///// �������� �������� � ��������� �� -2Pi �� 2Pi
		else
			for (int j = 0; j < amnt_pnt; j++)
				PntRefSig.Vec_RefSig[amnt_pnt * i + j] =
				_A * cos(2 * M_PI * (_fo * 1000) * ((amnt_pnt * i + j) / (_Fs * 1000 * amnt_pnt)));
	}

	//Noise(PntRefSig.Vec_RefSig);
	double max1 = 0;
	for (auto i : PntRefSig.Vec_RefSig)
		if (max1 < i)
			max1 = i;

	_xmaxSig1 = PntRefSig.Vec_RefSig.size();
	_yminSig1 = -1.5 * max1; _ymaxSig1 = 1.5 * max1;
	PntRefSig.Exchange(_xminSig1, _xmaxSig1, _yminSig1, _ymaxSig1, _xmaxSig1 / 8, _ymaxSig1 / 3);

	PntExplrSig.Vec_ExplrSig.clear();
	PntExplrSig.Vec_ExplrSig.resize(_N2 * amnt_pnt);
	// ����������� ������ � ���������� ��2
	for (int i = 0; i < _N2; i++)
	{
		if (vec_explrsig[i] == true)
			for (int j = 0; j < amnt_pnt; j++)
				PntExplrSig.Vec_ExplrSig[i * amnt_pnt + j] =
				_A * cos(2 * M_PI * (_fo * 1000) * ((amnt_pnt * i + j) / (_Fs * 1000 * amnt_pnt)) + M_PI);
		else
			for (int j = 0; j < amnt_pnt; j++)
				PntExplrSig.Vec_ExplrSig[i * amnt_pnt + j] =
				_A * cos(2 * M_PI * (_fo * 1000) * ((amnt_pnt * i + j) / (_Fs * 1000 * amnt_pnt)));
	}


	//Noise(PntExplrSig.Vec_ExplrSig);
	double max2 = 0;
	for (auto i : PntExplrSig.Vec_ExplrSig)
		if (max2 < i)
			max2 = i;

	_xmaxSig2 = PntExplrSig.Vec_ExplrSig.size();;
	_yminSig2 = -1.5 * max2; _ymaxSig2 = 1.5 * max2;
	PntExplrSig.Exchange(_xminSig2, _xmaxSig2, _yminSig2, _ymaxSig2, _xmaxSig2 / 8, _ymaxSig2 / 3);
}

/** �������� ��������� MSK.*/
void CCrossCorrelationDlg::ModulationMSK()
{
	/** ����� ����.*/
	double  theta = 0;
	/** ������� ��������.*/
	double delta_f = _Br / 4;
	int amnt_pnt = (int)(_T * _Fs * 1000);
	PntRefSig.Vec_RefSig.clear();
	PntRefSig.Vec_RefSig.resize(_N1 * amnt_pnt);
	// ������� ������ � ���������� ���
	for (int i = 0; i < _N1; i++)
	{
		if (vec_refsig[i] == true)
		{
			for (int j = 0; j < amnt_pnt; j++)
			{
				if (j == 0)
				{
					PntRefSig.Vec_RefSig[amnt_pnt * i + j] =
						_A * cos(2 * M_PI * (_fo * 1000 + delta_f) * ((amnt_pnt * i + j) / (_Fs * 1000 * amnt_pnt)) + theta);
					theta += 2 * M_PI * (_fo * 1000 + delta_f) * ((amnt_pnt * i + j) / (_Fs * 1000 * amnt_pnt));
				}
				else
				{
					PntRefSig.Vec_RefSig[amnt_pnt * i + j] =
						_A * cos(2 * M_PI * (_fo * 1000 + delta_f) * ((amnt_pnt * i + j) / (_Fs * 1000 * amnt_pnt)));
					theta += 2 * M_PI * (_fo * 1000 + delta_f) * ((amnt_pnt * i + j) / (_Fs * 1000 * amnt_pnt));
				}
			}
			theta *= vec_refsig[i];
		}
		else
		{
			for (int j = 0; j < amnt_pnt; j++)
			{
				if (j == 0)
				{
					PntRefSig.Vec_RefSig[amnt_pnt * i + j] =
						_A * cos(2 * M_PI * (_fo * 1000 - delta_f) * ((amnt_pnt * i + j) / (_Fs * 1000 * amnt_pnt)) + theta);
					theta += 2 * M_PI * (_fo * 1000 - delta_f) * ((amnt_pnt * i + j) / (_Fs * 1000 * amnt_pnt));
				}
				else
				{
					PntRefSig.Vec_RefSig[amnt_pnt * i + j] =
						_A * cos(2 * M_PI * (_fo * 1000 - delta_f) * ((amnt_pnt * i + j) / (_Fs * 1000 * amnt_pnt)));
					theta += 2 * M_PI * (_fo * 1000 - delta_f) * ((amnt_pnt * i + j) / (_Fs * 1000 * amnt_pnt));
				}
				theta *= vec_refsig[i];
			}
		}
	}

	//Noise(PntRefSig.Vec_RefSig);
	double max1 = 0;
	for (auto i : PntRefSig.Vec_RefSig)
		if (max1 < i)
			max1 = i;

	_xmaxSig1 = PntRefSig.Vec_RefSig.size();
	_yminSig1 = -1.5 * max1; _ymaxSig1 = 1.5 * max1;
	PntRefSig.Exchange(_xminSig1, _xmaxSig1, _yminSig1, _ymaxSig1, _xmaxSig1 / 8, _ymaxSig1 / 3);

	PntExplrSig.Vec_ExplrSig.clear();
	PntExplrSig.Vec_ExplrSig.resize(_N2 * amnt_pnt);
	theta = 0;
	// ����������� ������ � ���������� ���
	for (int i = 0; i < _N2; i++)
	{
		if (vec_explrsig[i] == true)
		{
			for (int j = 0; j < amnt_pnt; j++)
			{
				if (j == 0)
				{
					PntExplrSig.Vec_ExplrSig[i * amnt_pnt + j] =
						_A * cos(2 * M_PI * (_fo * 1000 + delta_f) * ((amnt_pnt * i + j) / (_Fs * 1000 * amnt_pnt)) + theta);
					theta += 2 * M_PI * (_fo * 1000 + delta_f) * ((amnt_pnt * i + j) / (_Fs * 1000 * amnt_pnt));
				}
				else
				{
					PntExplrSig.Vec_ExplrSig[i * amnt_pnt + j] =
						_A * cos(2 * M_PI * (_fo * 1000 + delta_f) * ((amnt_pnt * i + j) / (_Fs * 1000 * amnt_pnt)));
					theta += 2 * M_PI * (_fo * 1000 + delta_f) * ((amnt_pnt * i + j) / (_Fs * 1000 * amnt_pnt));
				}
				theta *= vec_explrsig[i];
			}
		}
		else
		{
			for (int j = 0; j < amnt_pnt; j++)
			{
				if (j == 0)
				{
					PntExplrSig.Vec_ExplrSig[i * amnt_pnt + j] =
						_A * cos(2 * M_PI * (_fo * 1000 - delta_f) * ((amnt_pnt * i + j) / (_Fs * 1000 * amnt_pnt)) + theta);
					theta += 2 * M_PI * (_fo * 1000 - delta_f) * ((amnt_pnt * i + j) / (_Fs * 1000 * amnt_pnt));
				}
				else
				{
					PntExplrSig.Vec_ExplrSig[i * amnt_pnt + j] =
						_A * cos(2 * M_PI * (_fo * 1000 - delta_f) * ((amnt_pnt * i + j) / (_Fs * 1000 * amnt_pnt)));
					theta += 2 * M_PI * (_fo * 1000 - delta_f) * ((amnt_pnt * i + j) / (_Fs * 1000 * amnt_pnt));
				}
				theta *= vec_explrsig[i];
			}
		}
	}

	//Noise(PntExplrSig.Vec_ExplrSig);
	double max2 = 0;
	for (auto i : PntExplrSig.Vec_ExplrSig)
		if (max2 < i)
			max2 = i;

	_xmaxSig2 = PntExplrSig.Vec_ExplrSig.size();;
	_yminSig2 = -1.5 * max2; _ymaxSig2 = 1.5 * max2;
	PntExplrSig.Exchange(_xminSig2, _xmaxSig2, _yminSig2, _ymaxSig2, _xmaxSig2 / 8, _ymaxSig2 / 3);
}

/** �������� ��� �� �������.*/
void CCrossCorrelationDlg::Noise(std::vector <double> & Vec_Buf)
{
	size_t size = Vec_Buf.size();
	// ��������� ����
	double sum_kv_n = 0;
	int M = 0;
	double *arr_n = new double[size];
	for (size_t i = 0; i < size; i++)
	{
		M = rand() % 9 + 12;
		double sum_Qsi = 0;
		for (int j = 0; j < M; j++)
		{
			sum_Qsi += (rand() % 21 - 10) / 10.;
		}
		arr_n[i] = sum_Qsi / M;
		sum_kv_n += arr_n[i] * arr_n[i];
	};

	double sum_kv_Sig = 0;
	for (size_t t = 0; t < size; t++)
	{
		sum_kv_Sig += Vec_Buf[t] * Vec_Buf[t];
	};

	double _alpha = 0;
	_alpha = sqrt(_d / 100 * sum_kv_Sig / (sum_kv_n));

	// ��������� ������� ���������� � �����
	for (size_t i = 0; i < size; i++)
	{
		if (_d != 0)
			Vec_Buf[i] = fabs(Vec_Buf[i] + _alpha*arr_n[i]);
		else
			Vec_Buf[i] = Vec_Buf[i];
	};

}

/** ��������� ���.*/
void CCrossCorrelationDlg::Rxy()
{
	PntCrssCrrltn.Vec_Crrltn.clear();
	/** ������ ���.*/
	size_t sizeCrrltn = PntExplrSig.Vec_ExplrSig.size() - PntRefSig.Vec_RefSig.size();

	double Val;
	for (size_t i = 0; i < sizeCrrltn; i++)
	{
		Val = 0;
		for (size_t j = 0; j < sizeCrrltn; j++)
			Val += PntRefSig.Vec_RefSig[j] * PntExplrSig.Vec_ExplrSig[j + i];
		if (Val < 0)
			Val = 0;
		else
			Val *= 1.f / sizeCrrltn;
		PntCrssCrrltn.Vec_Crrltn.push_back(Val);
	}

	double max = 0;
	for (auto i : PntCrssCrrltn.Vec_Crrltn)
		if (max < i)
			max = i;

	_xmaxSig3 = sizeCrrltn;
	_yminSig3 = -max - max / 2;	_ymaxSig3 = max + max / 2;
	PntCrssCrrltn.Exchange(_xminSig3, _xmaxSig3, _yminSig3, _ymaxSig3, _xmaxSig3 / 8, _ymaxSig3 / 3);
}

/** ���������� ��������� ��������.*/
void CCrossCorrelationDlg::OnBnClickedBtnsig()
{
	UpdateData(TRUE);

	CreateBitSignals();

	if (check_BPSK.GetCheck())
	{
		ModulationBPSK();
		Rxy();
	}
	else if (check_MSK.GetCheck())
	{
		ModulationBPSK();
		Rxy();
	}

	UpdateData(FALSE);
}
