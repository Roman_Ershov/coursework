#include "stdafx.h"
#include "PaintExplrSig.h"
#include "Gdiplus.h"

using namespace Gdiplus;

// ������������� GDI+
PaintExplrSig::PaintExplrSig()
{
	GdiplusStartupInput input;
	Status s;
	s = GdiplusStartup(&token, &input, NULL);
	if (s != Ok) MessageBox(L"Don't Open", L"Warning", MB_ICONERROR);
}

// ��������������� GDI+
PaintExplrSig::~PaintExplrSig()
{
	GdiplusShutdown(token);
}

// ������� ������ ����������� ����� ��������
void PaintExplrSig::Exchange(double left, double right, double low, double up, double step_setka_x, double step_setka_y)
{
	xmin = left;
	xmax = right;
	ymin = low;
	ymax = up;
	step_x = step_setka_x;
	step_y = step_setka_y;
	Invalidate();
}

// �������������� � � ����������� �������
double PaintExplrSig::Trans_X(LPDRAWITEMSTRUCT lpDrawItemStruct, double x)
{
	return (double)(lpDrawItemStruct->rcItem.right) / (double)(xmax - xmin)*((x)-xmin);
}

// �������������� � � ����������� �������
double PaintExplrSig::Trans_Y(LPDRAWITEMSTRUCT lpDrawItemStruct, double y)
{
	return -(double)(lpDrawItemStruct->rcItem.bottom) / (double)(ymax - ymin)*((y)-ymax);
}

// ������� ���������
void PaintExplrSig::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	Graphics paint(lpDrawItemStruct->hDC);
	Bitmap bmp(lpDrawItemStruct->rcItem.right, lpDrawItemStruct->rcItem.bottom, &paint);
	Graphics paint_virtual(&bmp);

	FontFamily font_Osi(L"Times New Roman");
	Gdiplus::Font font(&font_Osi, 10, FontStyleRegular, UnitPixel);

	SolidBrush brush(Color::Black);
	SolidBrush brush_dot(Color::Black);
	SolidBrush brush_pndlm(Color::Indigo);
	Pen Pen_Gr(Color::Brown, (Gdiplus::REAL)2);
	Pen Pen_Osi(Color::Black, (Gdiplus::REAL)0.1);
	Pen Pen_Setka(Color::DarkSlateGray, (Gdiplus::REAL)0.1);
	Pen_Setka.SetDashStyle(DashStyle::DashStyleDash);

	paint_virtual.SetSmoothingMode(SmoothingModeAntiAlias);
	paint_virtual.Clear(Color::LightGray);

	// ������� ���
	paint_virtual.DrawLine(&Pen_Osi, (Gdiplus::REAL)Trans_X(lpDrawItemStruct, xmin), (Gdiplus::REAL)Trans_Y(lpDrawItemStruct, 0),
		(Gdiplus::REAL)Trans_X(lpDrawItemStruct, xmax), (Gdiplus::REAL)Trans_Y(lpDrawItemStruct, 0));
	paint_virtual.DrawLine(&Pen_Osi, (Gdiplus::REAL)Trans_X(lpDrawItemStruct, 0), (Gdiplus::REAL)Trans_Y(lpDrawItemStruct, ymin),
		(Gdiplus::REAL)Trans_X(lpDrawItemStruct, 0), (Gdiplus::REAL)Trans_Y(lpDrawItemStruct, ymax));

	// �����
	// ������������ �����
	for (double x = 0; x <= xmax; x += step_x)
	{
		paint_virtual.DrawLine(&Pen_Setka, (Gdiplus::REAL)Trans_X(lpDrawItemStruct, x), (Gdiplus::REAL)Trans_Y(lpDrawItemStruct, ymin),
			(Gdiplus::REAL)Trans_X(lpDrawItemStruct, x), (Gdiplus::REAL)Trans_Y(lpDrawItemStruct, ymax));
	}

	// �������������� �����
	for (double y = 0; y <= ymax; y += ymax / 2)
	{
		paint_virtual.DrawLine(&Pen_Setka, (Gdiplus::REAL)Trans_X(lpDrawItemStruct, xmin), (Gdiplus::REAL)Trans_Y(lpDrawItemStruct, y),
			(Gdiplus::REAL)Trans_X(lpDrawItemStruct, xmax), (Gdiplus::REAL)Trans_Y(lpDrawItemStruct, y));
	}
	for (double y = 0; y >= ymin; y -= ymax / 2)
	{
		paint_virtual.DrawLine(&Pen_Setka, (Gdiplus::REAL)Trans_X(lpDrawItemStruct, xmin), (Gdiplus::REAL)Trans_Y(lpDrawItemStruct, y),
			(Gdiplus::REAL)Trans_X(lpDrawItemStruct, xmax), (Gdiplus::REAL)Trans_Y(lpDrawItemStruct, y));
	}

	CString str;

	// ������� ����� �� �����������
	for (double x = 0; x <= xmax; x += step_x)
	{
		str.Format(_T("%.0f"), x);
		paint_virtual.DrawString(str, -1, &font, PointF((Gdiplus::REAL)Trans_X(lpDrawItemStruct, x + 0.02),
			(Gdiplus::REAL)Trans_Y(lpDrawItemStruct, - ymax / 40)), NULL, &brush);
	}

	// ������� ����� �� ���������
	for (double y = 0; y <= ymax; y += ymax / 2)
	{
		if (y != 0)
		{
			str.Format(_T("%.1f"), y);
			paint_virtual.DrawString(str, -1, &font, PointF((Gdiplus::REAL)Trans_X(lpDrawItemStruct, (xmax / 100)),
				(Gdiplus::REAL)Trans_Y(lpDrawItemStruct, y - y / 20)), NULL, &brush);
		}
	}

	// ������� ����� �� ���������
	for (double y = 0; y >= ymin; y -= ymax / 2)
	{
		if (y != 0)
		{
			str.Format(_T("%.1f"), y);
			paint_virtual.DrawString(str, -1, &font, PointF((Gdiplus::REAL)Trans_X(lpDrawItemStruct, (xmax / 100)),
				(Gdiplus::REAL)Trans_Y(lpDrawItemStruct, y + y / 20)), NULL, &brush);
		}
	}

	if (!Vec_ExplrSig.empty())
	{
		size_t size = Vec_ExplrSig.size();
		int x1 = 0; double y1 = 0;
		int x2 = 0; double y2 = 0;
		for (size_t i = 1; i < size; i++)
		{
			x2 = i;
			y2 = Vec_ExplrSig[i];
			paint_virtual.DrawLine(&Pen_Gr, (Gdiplus::REAL)Trans_X(lpDrawItemStruct, x1), (Gdiplus::REAL)Trans_Y(lpDrawItemStruct, y1),
				(Gdiplus::REAL)Trans_X(lpDrawItemStruct, x2), (Gdiplus::REAL)Trans_Y(lpDrawItemStruct, y2));
			x1 = x2;
			y1 = y2;
		}
	}

	paint.DrawImage(&bmp, 0, 0);
}
