
// CrossCorrelationDlg.h : ���� ���������
//

#pragma once

#include "PaintRefSig.h"
#include "PaintExplrSig.h"
#include "PaintCrossCorrelation.h"
#include <vector>
#include "afxwin.h"

// ���������� ���� CCrossCorrelationDlg
class CCrossCorrelationDlg : public CDialogEx
{
// ��������
public:
	CCrossCorrelationDlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CROSSCORRELATION_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
private:
	double _xminSig1, _xmaxSig1,   // ����������� � ������������ ������� �� � ��� �������� �������
		_yminSig1, _ymaxSig1;   // ����������� � ������������ �������� �� � ��� �������� �������
	double _xminSig2, _xmaxSig2,   // ����������� � ������������ ������� �� � ��� ������������ �������
		_yminSig2, _ymaxSig2;   // ����������� � ������������ �������� �� � ��� ������������ �������
	double _xminSig3, _xmaxSig3,   // ����������� � ������������ ������� �� � ��� �������� ����������
		_yminSig3, _ymaxSig3;   // ����������� � ������������ �������� �� � ��� �������� ����������
	int _N1;   // ���������� ��� � ������� �������
	int _N2;   // ���������� ��� � ����������� �������
	double _A;   // ��������� �������� �/��� ������������ �������
	double _fo;   // ������� ������� �������� �/��� ������������ �������
	int _Br;   // �������� ��������
	double _Fs;   // ������� ������������� �������� �/��� ������������ �������
	double _T;   // ������ ���������
	double _d;  // ���
public:
	PaintRefSig PntRefSig;   // ������ ������ PaintRefSig
	PaintExplrSig PntExplrSig;   // ������ ������ PaintExplrSig
	PaintCrossCorrelation PntCrssCrrltn;   // ������ ������ PaintCrossCorrelation
	CButton check_BPSK;   // ������ checkbox BPSK
	CButton check_MSK;   // ������ checkbox MSK

	/** ������������� ������������ �������(������� � �����������).
	* ������� ����������� ����� ������������������ ���(0 � 1).*/
	void CreateBitSignals();
	std::vector <bool> vec_refsig;   // ������������ ������� ������
	std::vector <bool> vec_explrsig;   // ������������ ����������� ������
	/** �������� ��������� BPSK*/
	void ModulationBPSK();
	/** �������� ��������� MSK*/
	void ModulationMSK();
	/** ��������� ���*/
	void Rxy();
	/** �������� ��� �� �������.*/
	void Noise(std::vector <double> & Vec_Buf);
	/** ���������� ��������� ��������.*/
	afx_msg void OnBnClickedBtnsig();

};
